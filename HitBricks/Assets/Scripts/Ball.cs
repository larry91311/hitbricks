﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    Rigidbody2D ballRigidBody2D;
    [Header("水平速度")]
    public float speedX;
    [Header("重直速度")]
    public float speedY;

    #region 教學理解用 可不寫
    [Header("實際水平速度")]
    public float velocityX;
    [Header("實際垂直速度")]
    public float velocityY;
    #endregion

    enum tags
    {
        Brick,BackGround
    }
    // Start is called before the first frame update
    void Start()
    {
        ballRigidBody2D = GetComponent<Rigidbody2D>();
        ballRigidBody2D.velocity = new Vector2(speedX, speedY);

    }

    // Update is called once per frame
    void Update()
    {
        #region 教學理解用 可不寫
        velocityX = ballRigidBody2D.velocity.x;
        velocityY = ballRigidBody2D.velocity.y;
        #endregion
    }
    void lockSpeed()
    {
        Vector2 LockSpeed = new Vector2(resetSpeedX(), resetSpeedY());
        ballRigidBody2D.velocity = LockSpeed;
    }
    float resetSpeedX()
    {
        float currentSpeedX = ballRigidBody2D.velocity.x;
        if(currentSpeedX<0)
        {
            return -speedX;
        }
        else
        {
            return speedX;
        }
    }
    float resetSpeedY()
    {
        float currentSpeedY = ballRigidBody2D.velocity.y;
        if (currentSpeedY < 0)
        {
            return -speedY;
        }
        else
        {
            return speedY;
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        lockSpeed();
        if(other.gameObject.CompareTag(tags.Brick.ToString()))  //也可是("Brick")
        {
            other.gameObject.SetActive(false);
        }
    }
}
