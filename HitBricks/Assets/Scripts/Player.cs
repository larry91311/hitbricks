﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("水平移動速度")]
    public float speedX;
    Rigidbody2D playerRigidBody2D;

    

    // Start is called before the first frame update
    void Start()
    {
        playerRigidBody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        moveLeftOrRight();
    }

    float LeftOrRight()      //取得往左往右，+1到-1
    {
        return Input.GetAxis("Horizontal");
    }
    void moveLeftOrRight()
    {
        playerRigidBody2D.velocity = LeftOrRight() * new Vector2(speedX, 0);
    }
}
